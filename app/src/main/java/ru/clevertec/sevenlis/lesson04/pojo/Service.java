
package ru.clevertec.sevenlis.lesson04.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "serviceType",
    "description"
})

public class Service {

    @SerializedName("serviceType")
    public String serviceType;
    @SerializedName("description")
    public String description;

    public String getServiceType() {
        return serviceType;
    }

    public String getDescription() {
        return description;
    }
}
