package ru.clevertec.sevenlis.lesson04;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.clevertec.sevenlis.lesson04.pojo.Atm;
import ru.clevertec.sevenlis.lesson04.pojo.Root;

public class MapFragment extends SupportMapFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private FusedLocationProviderClient locationProviderClient;
    private GoogleMap mMap;

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        locationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getMapAsync(this);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;

        UiSettings settings = googleMap.getUiSettings();
        settings.setAllGesturesEnabled(true);
        settings.setZoomControlsEnabled(true);
        settings.setZoomGesturesEnabled(true);
        settings.setMyLocationButtonEnabled(true);

        if (getActivity() != null && MainActivity.isLocationPermissionGranted(getActivity()) && MainActivity.isLocationEnabled(getActivity())) {
            locationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location == null) {
                        requestLocation();
                    } else {
                        showMyPosition(location);
                    }
                }
            });
        } else {
            if (getActivity() != null && !MainActivity.isLocationPermissionGranted(getActivity())) {
                Toast.makeText(getActivity(), "Доступ к местоположению запрещен.", Toast.LENGTH_SHORT).show();
            } else if (getActivity() != null && !MainActivity.isLocationEnabled(getActivity())) {
                Toast.makeText(getActivity(), "Определение местоположения выключено.", Toast.LENGTH_SHORT).show();
            }
            Location location = new Location(LocationManager.PASSIVE_PROVIDER);
            location.setLatitude(52.42392D);
            location.setLongitude(31.01334D);
            showMyPosition(location);
        }

        AtmApiServiceHelper.getInstance().getAtmApiService().loadAtms().enqueue(new Callback<Root>() {
            @Override
            public void onResponse(@NonNull Call<Root> call, @NonNull Response<Root> response) {
                if (response.isSuccessful()) {
                    Root root = response.body();
                    if (root != null)
                        addAtmMarkers(root.getData().getAtms());

                } else if (getActivity() != null) {
                    ResponseBody errorBody = response.errorBody();
                    if (errorBody != null) {
                        try {
                            Toast.makeText(getActivity(), errorBody.string(), Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Root> call, @NonNull Throwable throwable) {
                if (getActivity() != null) {
                    Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        mMap.setOnMarkerClickListener(this);
    }

    @SuppressLint("MissingPermission")
    private void requestLocation() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000);
        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        showMyPosition(location);
                        locationProviderClient.removeLocationUpdates(this);
                    }
                }
            }
        };
        locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
    }

    private void showMyPosition(@NonNull Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.addMarker(new MarkerOptions().title("You are here").position(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15.0f),1000,null);
    }

    private void addAtmMarkers(List<Atm> atms) {
        BitmapDescriptor iconDescriptor = BitmapDescriptorFactory.fromResource(R.mipmap.ic_bsb);
        for (Atm atm : atms) {
            LatLng latLng = new LatLng(
                    Double.parseDouble(atm.getAddress().getGeolocation().getGeographicCoordinates().getLatitude()),
                    Double.parseDouble(atm.getAddress().getGeolocation().getGeographicCoordinates().getLongitude())
            );
            MarkerOptions markerOptions = new MarkerOptions()
                    .icon(iconDescriptor)
                    .position(latLng)
                    //.anchor(0.0f, 0.0f)
                    .title(atm.getAddress().getAddressName())
                    .snippet(atm.getAddress().getAddressLine());

            mMap.addMarker(markerOptions);
        }
    }


    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 20.0f),1000,null);
        marker.showInfoWindow();
        return true;
    }
}
