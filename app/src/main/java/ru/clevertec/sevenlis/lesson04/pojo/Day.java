
package ru.clevertec.sevenlis.lesson04.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "dayCode",
    "openingTime",
    "closingTime",
    "Break"
})

public class Day {

    @SerializedName("dayCode")
    public String dayCode;
    @SerializedName("openingTime")
    public String openingTime;
    @SerializedName("closingTime")
    public String closingTime;
    @SerializedName("Break")
    public Break _break;

    public String getDayCode() {
        return dayCode;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public Break getBreak() {
        return _break;
    }
}
