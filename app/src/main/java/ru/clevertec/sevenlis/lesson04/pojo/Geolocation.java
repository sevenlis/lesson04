
package ru.clevertec.sevenlis.lesson04.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "GeographicCoordinates"
})

public class Geolocation {

    @SerializedName("GeographicCoordinates")
    public GeographicCoordinates geographicCoordinates;

    public GeographicCoordinates getGeographicCoordinates() {
        return geographicCoordinates;
    }
}
