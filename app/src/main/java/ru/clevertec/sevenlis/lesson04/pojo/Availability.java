
package ru.clevertec.sevenlis.lesson04.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "access24Hours",
    "isRestricted",
    "sameAsOrganization",
    "StandardAvailability"
})

public class Availability {

    @SerializedName("access24Hours")
    public Boolean access24Hours;
    @SerializedName("isRestricted")
    public Boolean isRestricted;
    @SerializedName("sameAsOrganization")
    public Boolean sameAsOrganization;
    @SerializedName("StandardAvailability")
    public StandardAvailability standardAvailability;

    public Boolean getAccess24Hours() {
        return access24Hours;
    }

    public Boolean getRestricted() {
        return isRestricted;
    }

    public Boolean getSameAsOrganization() {
        return sameAsOrganization;
    }

    public StandardAvailability getStandardAvailability() {
        return standardAvailability;
    }
}
