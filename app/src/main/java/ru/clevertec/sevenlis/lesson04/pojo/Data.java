
package ru.clevertec.sevenlis.lesson04.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ATM"
})

public class Data {

    @SerializedName("ATM")
    public List<Atm> atms = new ArrayList<>();

    public List<Atm> getAtms() {
        return atms;
    }
}
