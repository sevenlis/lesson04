package ru.clevertec.sevenlis.lesson04;

import retrofit2.Call;
import retrofit2.http.GET;
import ru.clevertec.sevenlis.lesson04.pojo.Root;

public interface AtmApiService {
    @GET("atms")
    Call<Root> loadAtms();
}
