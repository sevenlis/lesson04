
package ru.clevertec.sevenlis.lesson04.pojo;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "breakFromTime",
    "breakToTime"
})

public class Break {

    @SerializedName("breakFromTime")
    public String breakFromTime;
    @SerializedName("breakToTime")
    public String breakToTime;

    public String getBreakFromTime() {
        return breakFromTime;
    }

    public String getBreakToTime() {
        return breakToTime;
    }
}
