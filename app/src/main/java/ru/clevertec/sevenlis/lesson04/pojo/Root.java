
package ru.clevertec.sevenlis.lesson04.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Data"
})

public class Root {

    @SerializedName("Data")
    public Data data;

    public Data getData() {
        return data;
    }
}
