package ru.clevertec.sevenlis.lesson04;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AtmApiServiceHelper {
    private static AtmApiServiceHelper mInstance;
    private static final String BASE_URL = "https://belarusbank.by/open-banking/v1.0/";
    private final Retrofit mRetrofit;

    public AtmApiServiceHelper() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public AtmApiService getAtmApiService() {
        return mRetrofit.create(AtmApiService.class);
    }

    public static AtmApiServiceHelper getInstance() {
        if (mInstance == null)
            mInstance = new AtmApiServiceHelper();
        return mInstance;
    }
}
